﻿using System;

namespace KMPG.Domain.Entities
{
    public class LeaderBoard : BaseEntity
    {
        public long Balance { get; set; }
        public DateTime LastUpdatedDate { get; set; }
    }
}
