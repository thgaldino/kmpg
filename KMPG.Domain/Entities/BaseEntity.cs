﻿
using System.ComponentModel.DataAnnotations;

namespace KMPG.Domain.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public virtual long PlayerId { get; set; }
    }
}
