﻿using System;

namespace KMPG.Domain.Entities
{
    public class GameResultEntity : BaseEntity
    {
        public long GameId { get; set; }
        public long Win { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
