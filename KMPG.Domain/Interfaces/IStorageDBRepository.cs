﻿using KMPG.Domain.Entities;
using System.Collections.Generic;

namespace KMPG.Domain.Interfaces
{
    public interface IStorageDBRepository<TEntity> where TEntity : BaseEntity
    {
        void Insert(TEntity obj);

        void Update(TEntity obj);

        void Delete(long playerId);

        IList<TEntity> Select();

        TEntity Select(long playerId);
    }
}
