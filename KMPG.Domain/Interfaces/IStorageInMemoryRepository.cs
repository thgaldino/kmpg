﻿using KMPG.Domain.Entities;
using System.Collections.Generic;

namespace KMPG.Domain.Interfaces
{
    public interface IStorageInMemoryRepository<TEntity> where TEntity : BaseEntity
    {
        void Insert(TEntity obj);

        IList<TEntity> Select();

        TEntity Select(long playerId);

        void Delete();
    }
}
