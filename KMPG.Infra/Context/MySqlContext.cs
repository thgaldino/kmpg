﻿using KMPG.Domain.Entities;
using KMPG.Infra.Mapping;
using Microsoft.EntityFrameworkCore;

namespace KMPG.Infra.Context
{
    public class MySqlContext : DbContext
    {
        public MySqlContext(DbContextOptions<MySqlContext> options) : base(options)
        {

        }

        public DbSet<GameResultEntity> GameResults { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<GameResultEntity>(new GameResultMap().Configure);
        }
    }
}
