﻿using KMPG.Domain.Entities;
using KMPG.Domain.Interfaces;
using KMPG.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace KMPG.Infra.Repository
{
    public class StorageDBRepository<TEntity> : IStorageDBRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly MySqlContext _context;

        public StorageDBRepository(MySqlContext context)
        {
            _context = context;
        }

        public void Insert(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
            _context.SaveChanges();
        }

        public void Update(TEntity obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(long playerId)
        {
            _context.Set<TEntity>().Remove(Select(playerId));
            _context.SaveChanges();
        }

        public IList<TEntity> Select() =>
            _context.Set<TEntity>().ToList();

        public TEntity Select(long playerId) =>
            _context.Set<TEntity>().Find(playerId);
    }
}
