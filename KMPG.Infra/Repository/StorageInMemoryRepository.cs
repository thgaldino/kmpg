﻿using KMPG.Domain.Entities;
using KMPG.Domain.Interfaces;
using KMPG.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace KMPG.Infra.Repository
{
    public class StorageInMemoryRepository<TEntity> : IStorageInMemoryRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly InMemoryContext _context;

        public StorageInMemoryRepository(InMemoryContext context)
        {
            _context = context;
        }

        public void Insert(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
            _context.SaveChanges();
        }

        public IList<TEntity> Select() =>
            _context.Set<TEntity>().ToList();

        public void Delete()
        {
            var list = _context.Set<TEntity>().ToList();
            _context.Set<TEntity>().RemoveRange(list);
            _context.SaveChanges();
        }

        public TEntity Select(long playerId) =>
            _context.Set<TEntity>().Find(playerId);
    }
}
