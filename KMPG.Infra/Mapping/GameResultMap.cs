﻿using KMPG.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KMPG.Infra.Mapping
{
    public class GameResultMap : IEntityTypeConfiguration<GameResultEntity>
    {
        public void Configure(EntityTypeBuilder<GameResultEntity> builder)
        {
            builder.ToTable("GameResult");

            builder.HasKey(prop => prop.PlayerId);

            builder.Property(prop => prop.GameId)
                .IsRequired()
                .HasColumnName("GameId")
                .HasColumnType("BIGINT");

            builder.Property(prop => prop.Win)
                .IsRequired()
                .HasColumnName("Win")
                .HasColumnType("BIGINT");

            builder.Property(prop => prop.Timestamp)
                .HasColumnName("TimeStamp")
                .HasColumnType("DATETIME");
        }
    }
}
