﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KMPG.Application.Models
{
    public class GameResultModel
    {
        [Key]
        public long PlayerId { get; set; }
        public long GameId { get; set; }
        public long Win { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
