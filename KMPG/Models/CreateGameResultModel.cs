﻿using System;

namespace KMPG.Application.Models
{
    public class CreateGameResultModel
    {
        public long GameId { get; set; }
        public long Win { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
