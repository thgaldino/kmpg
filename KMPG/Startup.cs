using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Pomelo.EntityFrameworkCore.MySql;

using KMPG.Infra.Context;
using KMPG.Domain.Interfaces;
using KMPG.Services.Services;
using KMPG.Domain.Entities;
using KMPG.Infra.Repository;
using KMPG.Application.Models;
using System;

namespace KMPG
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<MySqlContext>(options =>
            {
                var connection = Configuration["database:ConnectionString"];
                var server = Configuration["database:mysql:server"];
                var port = Configuration["database:mysql:port"];
                var database = Configuration["database:mysql:database"];
                var username = Configuration["database:mysql:username"];
                var password = Configuration["database:mysql:password"];

                options.UseMySql($"Server={server};Port={port};Database={database};Uid={username};Pwd={password}", new MySqlServerVersion(new Version(8, 0, 11)),opt =>
                {
                    opt.CommandTimeout(180);
                    opt.EnableRetryOnFailure(5);
                });
            });

            services.AddDbContext<InMemoryContext>(opt => opt.UseInMemoryDatabase("GameResult"));

            services.AddScoped<IStorageDBRepository<GameResultEntity>, StorageDBRepository<GameResultEntity>>();
            services.AddScoped<IStorageDBService<GameResultEntity>, StorageDBService<GameResultEntity>>();

            services.AddScoped<IStorageInMemoryRepository<GameResultEntity>, StorageInMemoryRepository<GameResultEntity>>();
            services.AddScoped<IStorageInMemoryService<GameResultEntity>, StorageInMemoryService<GameResultEntity>>();

            services.AddSingleton(new MapperConfiguration(config =>
            {
                config.CreateMap<CreateGameResultModel, GameResultEntity>();
                config.CreateMap<GameResultEntity, GameResultModel>();
            }).CreateMapper());

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "KMPG", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "KMPG v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
