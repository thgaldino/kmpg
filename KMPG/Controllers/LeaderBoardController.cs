﻿using KMPG.Infra.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.OData;

namespace KMPG.Application.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LeaderBoardController : ControllerBase
    {
        private readonly MySqlContext _context;

        public LeaderBoardController(MySqlContext context)
        {
            _context = context;
        }

        [HttpGet("{skip:int}/{take:int}")]
        [EnableQuery]
        public async Task<IActionResult> Get(
            [FromServices] MySqlContext context,
            int skip = 0,
            int take = 100)
        {
            var count = await context.GameResults.CountAsync();
            var result = await context
                .GameResults
                .AsNoTracking()
                .Skip(skip)
                .Take(take)
                .OrderByDescending(x => x.Win)
                .ToListAsync();

            return Ok(new
            {
                total = count,
                skip,
                take,
                data = result
            });
        }

    }
}

