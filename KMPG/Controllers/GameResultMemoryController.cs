﻿using Microsoft.AspNetCore.Mvc;
using System;

using KMPG.Domain.Interfaces;
using KMPG.Services.Validators;
using KMPG.Application.Models;
using KMPG.Domain.Entities;

namespace KMPG.Application.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GameResultMemoryController : ControllerBase
    {
        private readonly IStorageInMemoryService<GameResultEntity> _service;

        public GameResultMemoryController(IStorageInMemoryService<GameResultEntity> service)
        {
            _service = service;
        }

        [HttpPost]
        public IActionResult Create([FromBody] CreateGameResultModel gameResult)
        {
            if (gameResult == null)
                return NotFound();

            return Execute(() => _service.Add<CreateGameResultModel, GameResultModel, GameResultValidator>(gameResult));
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Execute(() => _service.Get<GameResultModel>());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            if (id == 0)
                return NotFound();

            return Execute(() => _service.GetById<GameResultModel>(id));
        }

        [HttpDelete("")]
        public IActionResult Delete()
        {
            Execute(() =>
            {
                _service.Delete();
                return true;
            });

            return new NoContentResult();
        }

        private IActionResult Execute(Func<object> func)
        {
            try
            {
                var result = func();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}

