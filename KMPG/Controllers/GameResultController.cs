﻿using Microsoft.AspNetCore.Mvc;
using System;

using KMPG.Domain.Interfaces;
using KMPG.Services.Validators;
using KMPG.Application.Models;
using KMPG.Domain.Entities;

namespace KMPG.Application.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GameResultController : ControllerBase
    {
        private readonly IStorageDBService<GameResultEntity> _service;

        public GameResultController(IStorageDBService<GameResultEntity> service)
        {
            _service = service;
        }

        [HttpPost]
        public IActionResult Create([FromBody] CreateGameResultModel gameResult)
        {
            if (gameResult == null)
                return NotFound();

            return Execute(() => _service.Add<CreateGameResultModel, GameResultModel, GameResultValidator>(gameResult));
        }

        [HttpPut]
        public IActionResult Update([FromBody] UpdateGameResultModel gameResult)
        {
            if (gameResult == null)
                return NotFound();

            return Execute(() => _service.Update<CreateGameResultModel, GameResultModel, GameResultValidator>(gameResult));
        }

        [HttpDelete("{playerId}")]
        public IActionResult Delete(long playerId)
        {
            if (playerId == 0)
                return NotFound();

            Execute(() =>
            {
                _service.Delete(playerId);
                return true;
            });

            return new NoContentResult();
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Execute(() => _service.Get<GameResultModel>());
        }

        [HttpGet("{id}")]
        public IActionResult Get(long playerId)
        {
            if (playerId == 0)
                return NotFound();

            return Execute(() => _service.GetById<GameResultModel>(playerId));
        }

        private IActionResult Execute(Func<object> func)
        {
            try
            {
                var result = func();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}

