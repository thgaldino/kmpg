﻿using KMPG.Application.Models;
using KMPG.Domain.Entities;
using KMPG.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace KMPG.WebJob
{
    public class EntryPoint : BackgroundService
    {
        private readonly ILogger<EntryPoint> _logger;
        private readonly IConfiguration _config;
        private readonly IHttpClientFactory _clientFactory;
        private readonly IStorageInMemoryService<GameResultEntity> _inMemoryservice;

        private const string HTTP_FACTORY_NAME = "KMPGAplication";

        public int PoolingInterval { get; set; }

        public int RetryDelay { get; private set; }

        public int MaxPoolingTimeDelay { get; private set; }

        public EntryPoint(IConfiguration config, IHttpClientFactory clientFactory,
            ILogger<EntryPoint> logger,
            IStorageInMemoryService<GameResultEntity> inMemoryservice)
        {
            _config = config;
            _clientFactory = clientFactory;
            _logger = logger;
            _inMemoryservice = inMemoryservice;

            this.PoolingInterval = 0;
            this.RetryDelay = 0;
            this.MaxPoolingTimeDelay = 0;
        }

        /// <summary>
        /// Entry Method
        /// </summary>
        /// <param name="stoppingToken">Cancellation token</param>
        /// <returns></returns>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Starting job storage data...");

            while (!stoppingToken.IsCancellationRequested)
                await Process();

            _logger.LogInformation("End job storage data...");
        }

        public async Task Process()
        {
            var endPointURL = _config[$"ApplicationAPI:EndPoints:GameResultDB:URL"];
            var getEndPointURL = _config[$"ApplicationAPI:EndPoints:GameResultMemory:URL"];

            var getDataMemory = this.Get(getEndPointURL);

            _logger.LogInformation($"{getDataMemory.Result.Count} Quantity founded.");


            foreach (var dataStorage in getDataMemory.Result)
            {
                await Post(dataStorage, endPointURL);
                _logger.LogInformation($"Dado armazenado com sucesso!");
                await Delete(getEndPointURL);
            }
            _logger.LogInformation("Finalizando processo de armazenamento com sucesso: {time}", DateTimeOffset.UtcNow);
            await this.DoBackoffDelay(getDataMemory.Result.Count);
        }

        private async Task<bool> Post(CreateGameResultModel gameResult, string postEndpointUrl)
        {
            bool success = false;
            try
            {
                using (var client = this._clientFactory.CreateClient(HTTP_FACTORY_NAME))
                {
                    var timeout = Convert.ToDouble(_config["AsyncPolicy:RequestTimeOut"]);
                    client.Timeout = TimeSpan.FromMilliseconds(timeout);

                    var horario = gameResult.Timestamp.ToString("yyyy-MM-ddTHH:mm:ss-03:00");

                    var gameResultViewModel = new
                    {
                        GameId = gameResult.GameId,
                        Win = gameResult.Win,
                        TimeStamp = horario,
                    };

                    var response = await client.PostAsync(postEndpointUrl, JsonContent.Create(gameResultViewModel));
                    success = response.IsSuccessStatusCode;

                    if (!success)
                    {
                        var message = await response.Content.ReadAsStringAsync();
                        _logger.LogError($"Application get error: {message}, status code: {response.StatusCode}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Application get error: {ex.Message}");
                success = false;
            }


            return success;
        }

        private async Task<List<CreateGameResultModel>> Get(string getEndpointUrl)
        {
            List<CreateGameResultModel> model = new List<CreateGameResultModel>();
            try
            {
                using (var client = this._clientFactory.CreateClient(HTTP_FACTORY_NAME))
                {
                    var timeout = Convert.ToDouble(_config["AsyncPolicy:RequestTimeOut"]);
                    client.Timeout = TimeSpan.FromMilliseconds(timeout);

                    var response = await client.GetAsync(getEndpointUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        var message = await response.Content.ReadAsStringAsync();

                        var messageToJson = JsonSerializer.Deserialize<List<CreateGameResultModel>>(message, new JsonSerializerOptions { PropertyNameCaseInsensitive = true});

                        _logger.LogInformation($"Application was Success: {message}, status code: {response.StatusCode}");

                        model = messageToJson;
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Application get error: {ex.Message}");
                return model;
            }
        }

        private async Task<bool> Delete(string postEndpointUrl)
        {
            bool success = false;
            try
            {
                using (var client = this._clientFactory.CreateClient(HTTP_FACTORY_NAME))
                {
                    var timeout = Convert.ToDouble(_config["AsyncPolicy:RequestTimeOut"]);
                    client.Timeout = TimeSpan.FromMilliseconds(timeout);

                    var response = await client.DeleteAsync(postEndpointUrl);
                    success = response.IsSuccessStatusCode;

                    if (!success)
                    {
                        var message = await response.Content.ReadAsStringAsync();
                        _logger.LogError($"Application get error: {message}, status code: {response.StatusCode}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Application get error: {ex.Message}");
                success = false;
            }


            return success;
        }

        private async Task DoBackoffDelay(int count)
        {
            if (count > 0)
            {
                this.PoolingInterval = 0;
                return;
            }
            this.PoolingInterval = PoolingInterval + (this.RetryDelay * 10);

            if (this.PoolingInterval > this.MaxPoolingTimeDelay)
                this.PoolingInterval = this.MaxPoolingTimeDelay;

            await Task.Delay(PoolingInterval);
        }
    }
}
