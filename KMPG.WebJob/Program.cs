﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;
using Polly.Contrib.WaitAndRetry;
using Polly.Extensions.Http;
using System;
using System.Data;
using System.Net.Http;

using KMPG.Application.Models;
using KMPG.Domain.Entities;
using KMPG.Domain.Interfaces;
using KMPG.Infra.Context;
using KMPG.Infra.Repository;
using KMPG.Services.Services;

namespace KMPG.WebJob
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                AppStartup(args).Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "Aplicação encerranda inesperadamente.");
            }
            finally
            {
                Console.WriteLine("Finalizando......");
            }
        }

        private static IHost AppStartup(String[] args)
        {
            var host = Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, configBuilder) =>
                {
                    configBuilder.Sources.Clear();

                    var builder = BuildConfig(hostingContext.HostingEnvironment.EnvironmentName);

                    Console.WriteLine($"Iniciando o serviço em:{hostingContext.HostingEnvironment.EnvironmentName}");
                    configBuilder.AddConfiguration(builder);
                })
                .ConfigureServices((context, services) =>
                {
                    SetServicesDI(services, context.Configuration);

                    services.AddHostedService<EntryPoint>();
                }).Build();
            return host;
        }

        public static void SetServicesDI(IServiceCollection services, IConfiguration configuration) 
        {
            services.AddSingleton(configuration);
            services.AddSingleton<EntryPoint>();

            services.AddDbContext<InMemoryContext>(opt => opt.UseInMemoryDatabase("GameResult"));

            services.AddScoped<IStorageInMemoryRepository<GameResultEntity>, StorageInMemoryRepository<GameResultEntity>>();
            services.AddScoped<IStorageInMemoryService<GameResultEntity>, StorageInMemoryService<GameResultEntity>>();

            services.AddScoped<IStorageDBRepository<GameResultEntity>, StorageDBRepository<GameResultEntity>>();
            services.AddScoped<IStorageDBService<GameResultEntity>, StorageDBService<GameResultEntity>>();

            services.AddSingleton(new MapperConfiguration(config =>
            {
                config.CreateMap<CreateGameResultModel, GameResultEntity>();
                config.CreateMap<GameResultEntity, GameResultModel>();
            }).CreateMapper());

            services.AddHttpClient("KMPGAplication", c =>  c.BaseAddress = new Uri(configuration["ApplicationAPI:EndPoints:BaseUrl"]));
                //.AddPolicyHandler(GetRetryPolicy(configuration));
        }
        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy(IConfiguration configuration)
        {
            var retryDelay = Convert.ToDouble(configuration["AsyncPolicy:RetryDelay"]);
            var retryCount = Convert.ToDouble(configuration["AsyncPolicy:RetryCount"]);

            var delay = Backoff.DecorrelatedJitterBackoffV2(medianFirstRetryDelay: TimeSpan.FromMilliseconds(retryDelay), retryCount: (int)retryCount);

            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
                .WaitAndRetryAsync(delay);
        }
        private static IConfigurationRoot BuildConfig(string environmentName) 
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
            return builder;
        }
    }
}
