﻿using FluentValidation;
using KMPG.Domain.Entities;
using KMPG.Domain.Interfaces;
using System;
using System.Collections.Generic;
using AutoMapper;
using System.Linq;

namespace KMPG.Services.Services
{
    public class StorageInMemoryService<TEntity> : IStorageInMemoryService<TEntity> where TEntity : BaseEntity
    {
        private readonly IStorageInMemoryRepository<TEntity> _repository;
        private readonly IMapper _mapper;

        public StorageInMemoryService(IStorageInMemoryRepository<TEntity> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public TOutputModel Add<TInputModel, TOutputModel, TValidator>(TInputModel inputModel)
            where TValidator : AbstractValidator<TEntity>
            where TInputModel : class
            where TOutputModel : class
        {
            TEntity entity = _mapper.Map<TEntity>(inputModel);

            Validate(entity, Activator.CreateInstance<TValidator>());
            _repository.Insert(entity);

            TOutputModel outputModel = _mapper.Map<TOutputModel>(entity);

            return outputModel;
        }

        public IEnumerable<TOutputModel> Get<TOutputModel>() where TOutputModel : class
        {
            var entities = _repository.Select();

            var outputModels = entities.Select(s => _mapper.Map<TOutputModel>(s));

            return outputModels;
        }

        public TOutputModel GetById<TOutputModel>(long playerId) where TOutputModel : class
        {
            var entity = _repository.Select(playerId);

            var outputModel = _mapper.Map<TOutputModel>(entity);

            return outputModel;
        }

        public void Delete() => _repository.Delete();

        private void Validate(TEntity obj, AbstractValidator<TEntity> validator)
        {
            if (obj == null)
                throw new Exception("Not find Registers!");

            validator.ValidateAndThrow(obj);
        }
    }
}
