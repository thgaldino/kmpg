﻿using AutoMapper;
using FluentValidation;
using KMPG.Domain.Entities;
using KMPG.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KMPG.Services.Services
{
    public class StorageDBService<TEntity> : IStorageDBService<TEntity> where TEntity : BaseEntity
    {
        private readonly IStorageDBRepository<TEntity> _repository;
        private readonly IMapper _mapper;


        public StorageDBService(IStorageDBRepository<TEntity> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public TOutputModel Add<TInputModel, TOutputModel, TValidator>(TInputModel inputModel)
            where TValidator : AbstractValidator<TEntity>
            where TInputModel : class
            where TOutputModel : class
        {
            TEntity entity = _mapper.Map<TEntity>(inputModel);

            Validate(entity, Activator.CreateInstance<TValidator>());
            _repository.Insert(entity);

            TOutputModel outputModel = _mapper.Map<TOutputModel>(entity);

            return outputModel;
        }

        public void Delete(long playerId) => _repository.Delete(playerId);

        public IEnumerable<TOutputModel> Get<TOutputModel>() where TOutputModel : class
        {
            var entities = _repository.Select();

            var outputModels = entities.Select(s => _mapper.Map<TOutputModel>(s));

            return outputModels;
        }

        public TOutputModel GetById<TOutputModel>(long playerId) where TOutputModel : class
        {
            var entity = _repository.Select(playerId);

            var outputModel = _mapper.Map<TOutputModel>(entity);

            return outputModel;
        }

        public TOutputModel Update<TInputModel, TOutputModel, TValidator>(TInputModel inputModel)
            where TValidator : AbstractValidator<TEntity>
            where TInputModel : class
            where TOutputModel : class
        {
            TEntity entity = _mapper.Map<TEntity>(inputModel);

            Validate(entity, Activator.CreateInstance<TValidator>());
            _repository.Insert(entity);

            TOutputModel outputModel = _mapper.Map<TOutputModel>(entity);

            return outputModel;
        }

        private void Validate(TEntity obj, AbstractValidator<TEntity> validator)
        {
            if (obj == null)
                throw new Exception("Not find Registers!");

            validator.ValidateAndThrow(obj);
        }
    }
}
