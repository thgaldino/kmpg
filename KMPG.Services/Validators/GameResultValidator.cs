﻿using FluentValidation;
using KMPG.Domain.Entities;

namespace KMPG.Services.Validators
{
    public class GameResultValidator : AbstractValidator<GameResultEntity>
    {
        public GameResultValidator()
        {
            RuleFor(c => c.Win)
                .NotEmpty().WithMessage("Please enter the win.")
                .NotNull().WithMessage("Please enter the win.");
        }
    }
}
